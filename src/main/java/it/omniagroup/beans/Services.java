package it.omniagroup.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.omniagroup.exceptions.BadRequest;
import it.omniagroup.exceptions.NoDataFound;
import it.omniagroup.model.Password;
import it.omniagroup.model.PasswordRepository;
import it.omniagroup.model.User;
import it.omniagroup.model.UserRepository;
import it.omniagroup.pojo.CreateRequest;
import it.omniagroup.pojo.LoginRequest;
import it.omniagroup.pojo.LoginResponse;

/*
 * Servizi di logica di business separati dagli endpoint del controller rest ma richiamati da esso.
 */

@Service
public class Services { 

	Logger logger = LoggerFactory.getLogger(Services.class);
	
	@Autowired
	private PasswordRepository passwordRepository;
	@Autowired
	private UserRepository userRepository;
	
	private String messaggio;
	
	public String getMessaggio() {
		return messaggio;
	}
	
	public void setMessaggio(String messaggio) {
		this.messaggio = messaggio;
	}
	
	/*
	 * Innesca il controllo e la validazione dei dati ed effettua il salvataggio in persistenza se corretti.
	 * in ingresso accetta un POJO CreateRequest 
	 */
	public Map<String, String> createUser(CreateRequest user) {
		setMessaggio("Salvato");
		HashMap<String, String> response = new HashMap<String, String>();
		User nuovoUtente = new User();
		logger.info("Verifica esistenza utente: ...");
		if (checkUserName(nuovoUtente, user.getUserName()))
		{
			nuovoUtente.setNome(user.getNome());
			nuovoUtente.setCognome(user.getCognome());
			nuovoUtente.setEmail(user.getEmail());
			logger.info("Verifica esistenza codicefiscale: ...");
			if (checkCodiceFiscale(nuovoUtente, user.getCodiceFiscale())) {
				userRepository.save(nuovoUtente);
				Password password = new Password();
				password.setUsernameid(nuovoUtente.getId());
				password.setPassword(user.getPassword());
				passwordRepository.save(password);
				logger.info("Dati Salvati con successo.");
			}
		} 
		response.put("Message", messaggio);
		return response;
	}
	
	/*
	 * Controllo e validazione dell'univocità dell'username rispetto l'intero sistema.
	 * In ingresso vuole l'oggetto User da popolare e la stringa con la username da validare
	 */
	private boolean checkUserName(User user, String username) {
		List<String> utenti = new ArrayList<String>();
		userRepository.findAll().forEach(users -> utenti.add(users.getUserName()));
		if (utenti.stream().anyMatch(oggetto -> username.equals(oggetto))) {
			logger.info("                           ... KO - Nome utente già presente");
			throw new BadRequest("Nome Utente già presente");
			//return false;
		} else
			logger.info("                           ... OK");
			user.setUserName(username);
		return true;
	}

	/*
	 * Controllo e validazione dell'univocità del codice fiscale rispetto l'intero sistema.
	 * In ingresso vuole l'oggetto User da popolare e la stringa con il codice fiscale da validare.
	 * La validazione del codice fiscale viene fatta anche sul pattern che dovrà essere formalmente valido. 
	 */
	private boolean checkCodiceFiscale(User user, String codiceFiscale) {
		if (codiceFiscale.toUpperCase().matches("^[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z]$")) {
			List<String> codiciFiscali = new ArrayList<String>();
			userRepository.findAll().forEach(users -> codiciFiscali.add(users.getCodiceFiscale()));

			if (codiciFiscali.stream().anyMatch(oggetto -> codiceFiscale.equals(oggetto))) {
				logger.info("                                  ... KO - Codice fiscale già presente");
				throw new BadRequest("Codice Fiscale già presente");
				//return false;
			} else
				logger.info("                                  ... OK");
				user.setCodiceFiscale(codiceFiscale);
			return true;
		} else {
			logger.info("								  ... KO - Codice Fiscale non formalmente valido");
			throw new BadRequest("Codice Fiscale non formalmente valido");
			//return false;
		}
	}
	
	/*
	 * Innesca il controllo e la validazione dei dati per la login.
	 * in ingresso accetta un POJO LoginRequest 
	 */
	public LoginResponse login(LoginRequest request) 
	{
		return retrieveAndLogUser(request);
	}
	
	/* 
	 * Effettua una query su Utenti per verificare l'utente 
	 * poi controlla in join utente e passwords (criptate) per l'autenticazione effettiva
	 */
	private LoginResponse retrieveAndLogUser(LoginRequest request)
	{	
		LoginResponse response = new LoginResponse();
		
		User user = userRepository.findUserByUsername(request.getUserName());
		
		if (user!=null && passwordRepository.findPasswordById(user.getId(), request.getPassword())!= null)
		{
			logger.info("Tentativo di login: OK");
			response.setUser(user);
			response.setMessaggio("Logged In");
		}
		else
		{
			logger.info("Tentativo di login: KO - Nome Utente o password errati");
			//response.setUser(new User());
			//response.setMessaggio("Nome Utente o password errati");
			throw new NoDataFound("Nome Utente o password errati");
		}		
		return response;
	}
	
}
