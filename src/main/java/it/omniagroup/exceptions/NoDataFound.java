package it.omniagroup.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Data not found")
public class NoDataFound extends RuntimeException {
	   public NoDataFound() {
	        super();
	    }
	    public NoDataFound(String message, Throwable cause) {
	        super(message, cause);
	    }
	    public NoDataFound(String message) {
	        super(message);
	    }
	    public NoDataFound(Throwable cause) {
	        super(cause);
	    }    
}
