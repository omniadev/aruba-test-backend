package it.omniagroup.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Request not formally valid")
public class BadRequest extends RuntimeException {
	   public BadRequest() {
	        super();
	    }
	    public BadRequest(String message, Throwable cause) {
	        super(message, cause);
	    }
	    public BadRequest(String message) {
	        super(message);
	    }
	    public BadRequest(Throwable cause) {
	        super(cause);
	    }
}

