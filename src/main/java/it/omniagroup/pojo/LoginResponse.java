package it.omniagroup.pojo;

import javax.annotation.ManagedBean;

import it.omniagroup.model.User;

/*
 * POJO per la response di login utente.
 */

@ManagedBean
public class LoginResponse  {
	
	private String messaggio;
	
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getMessaggio() {
		return messaggio;
	}

	public void setMessaggio(String messaggio) {
		this.messaggio = messaggio;
	}

	
	
}
