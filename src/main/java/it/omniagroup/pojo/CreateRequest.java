package it.omniagroup.pojo;

import javax.annotation.ManagedBean;

import org.apache.commons.codec.digest.DigestUtils;

/*
 * POJO per la request di creazione utente.
 */

@ManagedBean
public class CreateRequest  {
	
	private String userName;
	
	private String password;

	private String nome;

	private String cognome;

	private String codiceFiscale;

	private String email;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	//la password la salvo nativamente con la codifica in base64
	public void setPassword(String password) {
		this.password = new DigestUtils("SHA3-256").digestAsHex(password);
	}	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
	
}
