package it.omniagroup.pojo;

import javax.annotation.ManagedBean;

import org.apache.commons.codec.digest.DigestUtils;

/*
 * POJO per la request di login utente.
 */

@ManagedBean
public class LoginRequest  {
	
	private String userName;
	
	private String password;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	//Le password si salva nativamente con la codifica in base64, in modo da non passare mai i valori in chiaro
	public void setPassword(String password) {
		this.password = new DigestUtils("SHA3-256").digestAsHex(password);
	}	
	
}
