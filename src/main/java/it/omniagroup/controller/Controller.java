package it.omniagroup.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import it.omniagroup.beans.Services;
import it.omniagroup.exceptions.BadRequest;
import it.omniagroup.exceptions.NoDataFound;
import it.omniagroup.pojo.CreateRequest;
import it.omniagroup.pojo.LoginRequest;
import it.omniagroup.pojo.LoginResponse;

/*
 * Controller principale dell'architettura MVC di Spring. 
 * I path sono definiti nelle spring properties.
 */

@RestController 
@RequestMapping(path = "${controller.base.path}") 
public class Controller {
	
	Logger logger = LoggerFactory.getLogger(Controller.class);
	
	@Autowired
	private Services services;

	/* 
	 * API REST che innesca la registrazione dell'utente.
	 * in ingresso accetta un JSON contenente:
	 * 	String userName
	 *  String password;
	 *	String nome;
	 *	String cognome;
	 *	String codiceFiscale;
	 *	String email; 	
	 * la response è un json composto come segue:
	 *  String messaggio;
	 */
	@PostMapping(path = "${controller.create.path}") // Map ONLY POST Requests
	public @ResponseBody Map<String, String> addNewUser(@RequestBody CreateRequest user) {
		logger.info("Inizio creazione nuovo utente: \n"
				+ "	userName:"+user.getUserName()+"\n"
				+ "	nome:"+user.getNome()+"\n"
				+ "	cognome:"+user.getCognome()+"\n"
				+ "	codice fiscale:"+user.getCodiceFiscale());
		try{
			return services.createUser(user);
		}
		catch (BadRequest br )
		{ 
			throw br;
		}
	}

	/* 
	 * API REST che innesca la login.
	 * in ingresso accetta un JSON contenente:
	 * 	String userName
	 *  String password;	 
	 * la response è un json composto come segue:
	 *	String messaggio;
	 *  User user; 
	 * dove user è un record dell'entità UTENTI associata all'username loggato.    	 	
	 */
	@PostMapping(path = "${controller.login.path}")
	public @ResponseBody LoginResponse login(@RequestBody LoginRequest request) {
		logger.info("Tentativo di login per utente: "+request.getUserName());
		try{
			return services.login(request);
		}
		catch (NoDataFound ndf)
		{
			throw ndf;			
		}
	}
		
	//TODO - Migliorare gestione delle eccezioni per inviare response mirate. 

}