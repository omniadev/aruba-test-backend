package it.omniagroup.controller;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/*
 * Configurazioni interne per la gestione degli interceptor custom
 */

@EnableWebMvc
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override  
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new RequestInterceptor());
    }
}