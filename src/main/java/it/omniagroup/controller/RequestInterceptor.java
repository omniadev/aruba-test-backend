package it.omniagroup.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/*
 * Interceptor custom per la tracciatura su log prima e dopo di gestire la chiamata dal controller
 */

public class RequestInterceptor implements HandlerInterceptor {

	Logger logger = LoggerFactory.getLogger(RequestInterceptor.class);
	
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) {

        logger.info(request.getMethod() + " request in ingresso da: "+ request.getRemoteAddr() + " - " + request.getRequestURI());
        return true;
    }
    
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			@Nullable ModelAndView modelAndView) throws Exception {
    	
    	logger.info("Response status code: "+ response.getStatus());
	}
}