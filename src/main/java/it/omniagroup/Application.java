package it.omniagroup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/*
 * Classe principale di Spring per la creazione della app. Con la definizione del classpath per i componenti e delle entità in uso.
 */

@SpringBootApplication
@ComponentScan({"it.omniagroup"})
@EntityScan("it.omniagroup.model")
@EnableJpaRepositories("it.omniagroup.model")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
