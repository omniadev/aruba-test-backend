package it.omniagroup.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/*
 * JPA repository per la tabella UTENTI.
 * Definisce la query per il recuperod del singolo utente
 */

public interface UserRepository extends CrudRepository<User, Integer> {

	@Query("SELECT u FROM User u WHERE u.userName = ?1")
	User findUserByUsername(String username);
	
}
