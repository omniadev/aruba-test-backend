package it.omniagroup.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


/*
 * JPA repository per la tabella PASSWORD.
 * Definisce la query per il controllo della password sul singolo utente
 */
public interface PasswordRepository extends CrudRepository<Password, Integer> {

	@Query("SELECT u FROM Password u WHERE u.usernameid = ?1 and u.password = ?2")
	Password findPasswordById(Integer id, String password);
	
}

