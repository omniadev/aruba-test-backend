package it.omniagroup.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/*
 * Classe Entità della tabella PASSWORD. Se tale tabella non esiste verrà creata contestualmente all'avvio della app.
 */

@Entity
@Table(name = "PASSWORD")
public class Password {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	
	@NotNull	
	private Integer usernameid;
	
	@NotNull
	private String password;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUsernameid() {
		return usernameid;
	}

	public void setUsernameid(Integer usernameid) {
		this.usernameid = usernameid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}	
}
